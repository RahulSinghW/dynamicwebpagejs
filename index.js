//Getting elements from html
const balanceElement = document.getElementById("balance")
const getALoanBtnElement = document.getElementById("getALoanBtn")
const currLoanElement = document.getElementById("currLoan")
const workBtnElement = document.getElementById("workBtn")
const payElement = document.getElementById("pay")
const bankBtnElement = document.getElementById("bankBtn")
const repayLoanFromPayBtnElement = document.getElementById("repayLoanFromPayBtn")
const laptopsElement = document.getElementById("laptops")
const specsElement = document.getElementById("specs")
const titleElement = document.getElementById("title")
const descriptionElement = document.getElementById("description")
const priceElement = document.getElementById("price")
const buyLaptopBtnElement = document.getElementById("buyLaptopBtn")
const imageElement = document.getElementById("image")

let currBalance = 0
let currLoan = 0
let pay = 0
let laptops = []
let laptopPrice = 0
const apiUrl = 'https://noroff-komputer-store-api.herokuapp.com/'

//getting response from webapi
fetch(apiUrl+'computers')
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops))

//Adding items to dropdown list
const addLaptopsToMenu = (laptops) =>{
    laptops.forEach(x => addLaptopToMenu(x))
    specsElement.innerText = laptops[0].specs
    titleElement.innerText = laptops[0].title
    descriptionElement.innerText = laptops[0].description
    priceElement.innerText = laptops[0].price + " NOK"
    imageElement.src = apiUrl + laptops[0].image
    laptopPrice = parseInt(laptops[0].price)
}
const addLaptopToMenu = (laptop) =>{
    const laptopElement = document.createElement("option")
    laptop.value = laptop.id
    laptopElement.appendChild(document.createTextNode(laptop.title))
    laptopsElement.appendChild(laptopElement)
}

//Changes values based on if item in menu has been changed
const handleLaptopMenuChange = e =>{
    const selectedLaptop = laptops[e.target.selectedIndex]
    specsElement.innerText = selectedLaptop.specs
    titleElement.innerText = selectedLaptop.title
    descriptionElement.innerText = selectedLaptop.description
    priceElement.innerText = selectedLaptop.price + " NOK"
    imageElement.src = apiUrl + selectedLaptop.image
    laptopPrice = parseInt(selectedLaptop.price)
}

//Function to get a loan. Has to fulfill certain requirements to get a loan
const getAloan = () =>{
    if (currBalance <= 0){
        alert('Hmmmm, cannot give you a loan, cuz you broke')
    }
    else if(currLoan > 0){
        alert('Can receive loan when current loan is payed back. ' + 'Current loan = ' + currLoan)
    }
    else {
        let amountLoan = prompt('You can get a loan upto ' + currBalance*2 + 'Kr')
        if(amountLoan > currBalance*2){
            alert('Amount is too high')
        }
        else if (amountLoan == 0){
            alert('Amount cannot be 0')
        }
        else {
            currBalance += parseInt(amountLoan)
            currLoan = parseInt((amountLoan))
            alert('Loan added to your balance')
            balanceElement.innerText = 'Balance: ' + currBalance.toString() + 'Kr'
            currLoanElement.innerText = 'Current loan: ' + currLoan.toString() + 'Kr'
        }
    }
}

//Each click on work button gives 100Kr
const work = () =>{
    pay += 100;
    payElement.innerText = 'Pay: ' + pay + 'Kr'
}

//Save your work earnings in bank. If a loan i active 10% of the earnings will go to the bank
const bankEarnings = () =>{
    const loanDeduction = pay * 0.10
    const toAccountAfterDeduction = (pay * 0.90)
    if(currLoan > 0){
        if(loanDeduction > currLoan){
            const rest = loanDeduction-currLoan
            currLoan -= (loanDeduction - rest)
            currBalance -= (loanDeduction - rest)
            currBalance += (toAccountAfterDeduction + rest)
        }
        else {
            currLoan -= loanDeduction
            currBalance += toAccountAfterDeduction
        }
        currLoanElement.innerText = 'Current loan: ' + currLoan.toString() + 'Kr'
    }
    else {
        currBalance += pay
    }
    pay = 0;
    balanceElement.innerText = 'Balance: ' + currBalance.toString() + 'Kr'
    payElement.innerText = 'Pay: ' + pay + 'Kr'

}

//Repay loan directly from work.
const repayLoan = () =>{
    if(currLoan < pay){
        pay -= currLoan
        currLoan = 0
    }
    else {
        currLoan -= pay
        pay = 0
    }
    currLoanElement.innerText = 'Current loan: ' + currLoan.toString() + 'Kr'
    payElement.innerText = 'Pay: ' + pay + 'Kr'
}

//Function to buy a laptop
const buyLaptop = () => {
  if(laptopPrice > currBalance){
      alert('Cannot afford this laptop')
  }
  else{
      currBalance -= laptopPrice
      console.log(currBalance)
      alert('Congrats you bought a new laptop')
      balanceElement.innerText = 'Balance: ' + currBalance.toString() + 'Kr'
  }
}

//Eventlisteners
getALoanBtnElement.addEventListener('click', getAloan)
workBtnElement.addEventListener('click', work)
bankBtnElement.addEventListener('click', bankEarnings)
repayLoanFromPayBtnElement.addEventListener('click', repayLoan)
laptopsElement.addEventListener('change', handleLaptopMenuChange)
buyLaptopBtnElement.addEventListener('click', buyLaptop)